#! /usr/bin/env python

import argparse
from astropy.io import fits


def compute(args):
    data1 = fits.getdata(args.in1)
    data2 = fits.getdata(args.in2)
    if args.operation == "add":
        out_data = data1 + data2
    elif args.operation == "sub":
        out_data = data1 - data2
    elif args.operation == "div":
        out_data = data1 / data2
    elif args.operation == "mul":
        out_data = data1 * data2
    if args.copy_header:
        header1 = fits.getheader(args.in1)
        fits.PrimaryHDU(data=out_data, header=header1).writeto(args.out, overwrite=True)
    else:
        fits.PrimaryHDU(data=out_data).writeto(args.out, overwrite=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("in1", help="First fits file")
    parser.add_argument("in2", help="Second fits file")
    parser.add_argument("out", help="Name of output file")
    parser.add_argument("--operation", choices=["add", "sub", "mul", "div"], help="Operation to perform")
    parser.add_argument("--copy_header", action="store_true",
                        help="Copy header of the first fits to the output file")
    args = parser.parse_args()
    compute(args)
